#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import shape_msgs
import json
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from tf.transformations import quaternion_from_euler

from moveit_planner import Planner
from moveit_scene import Scene


########################    PLANNER SETUP AND INITIALIZATION    #####################

name = "manipulator"
planner = Planner(name)

########################        BOXES AS WORKSPACE BOUNDS       #####################

scene = Scene()

limx = (-0.3,0.7)
limy = (-0.4,0.4)
limz = (0.0,2.0)
scene.workspace(limx,limy,limz)
 
raw_input("PRESS A KEY TO CONTINUE")

########################        INITIAL CONFIGURATION          #####################

joint = [0]*6

joint[0] = 0
joint[1] = -pi/2
joint[2] = pi/2
joint[3] = -pi/2
joint[4] = -pi/2
joint[5] = 0

planner.go_to_joint_state(joint)

raw_input("PRESS A KEY TO CONTINUE")
 

########################        GO TO GOAL POSE          #####################

q = quaternion_from_euler(1.5707, 0, -1.5707)

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.x = q[0]
pose_goal.orientation.y = q[1]
pose_goal.orientation.z = q[2]
pose_goal.orientation.w = q[3]

pose_goal.position.x = 0.6
pose_goal.position.y = 0.1
pose_goal.position.z = 0.43

waypoints = [pose_goal] 

cartesian_plan, fraction = planner.plan_cartesian_path(waypoints)

raw_input("PRESS A KEY TO CONTINUE")

planner.display_trajectory(cartesian_plan)

raw_input("PRESS A KEY TO CONTINUE")

planner.go_to_pose_goal(pose_goal)