#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import shape_msgs
import json
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from tf.transformations import quaternion_from_euler

from moveit_planner import Planner
from moveit_scene import Scene


########################    PLANNER SETUP AND INITIALIZATION    #####################

name = "manipulator"
planner = Planner(name)

########################        BOXES AS WORKSPACE BOUNDS       #####################

scene = Scene()

limx = (-0.3,0.7)
limy = (-0.4,0.4)
limz = (0.0,2.0)
scene.workspace(limx,limy,liz)
 
