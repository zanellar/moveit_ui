#!/usr/bin/env python
import rospy
from moveit_commander import RobotCommander, PlanningSceneInterface
import geometry_msgs.msg
import time
import sys

class Scene(object):

    def __init__(self):
        self._scene = PlanningSceneInterface()

        # clear the scene
        self._scene.remove_world_object()

        self.robot = RobotCommander()

        # pause to wait for rviz to load
        print "============ Waiting while RVIZ displays the scene with obstacles..."

        # TODO: need to replace this sleep by explicitly waiting for the scene to be updated.
        rospy.sleep(2)

    def workspace(self, dx,dy,dz, scale=1):  # TODO add a positioning param
        l = scale*2
        v_plane_size = [l, 0.01, l]
        h_plane_size = [l, l, 0.01]

        minx = dx[0]
        maxx = dx[1]
        miny = dy[0]
        maxy = dy[1]
        minz = dz[0]
        maxz = dz[1]

        #front  
        if minx is not None:   
            pose = [minx, 0, 0, 0, 0, 0, 1]
            size = [0.01, l, l] 
            self.box("minx", size, pose)
        #back 
        if maxx is not None: 
            pose = [maxx, 0, 0, 0, 0, 0, 1]
            size = [0.01, l, l]
            self.box("maxx", size, pose)
        #left 
        if miny is not None: 
            pose = [0, miny, 0, 0, 0, 0, 1]
            size = [l, 0.01, l]
            self.box("miny", size, pose)
        #right  
        if maxy is not None: 
            pose = [0, maxy, 0, 0, 0, 0, 1]
            size = [l, 0.01, l]
            self.box("maxy", size, pose)
        #bottom 
        if minz is not None: 
            pose = [0, 0, minz, 0, 0, 0, 1]
            size = [l, l, 0.01]
            self.box("minz", size, pose)
        #top 
        if maxz is not None: 
            pose = [0, 0, maxz, 0, 0, 0, 1]
            size = [l, l, 0.01]
            self.box("maxz", size, pose) 

    def box(self, name, dimensions, pose):
        p = geometry_msgs.msg.PoseStamped()
        p.header.frame_id = self.robot.get_planning_frame()
        p.header.stamp = rospy.Time.now()
        p.pose.position.x = pose[0]
        p.pose.position.y = pose[1]
        p.pose.position.z = pose[2]
        p.pose.orientation.x = pose[3]
        p.pose.orientation.y = pose[4]
        p.pose.orientation.z = pose[5]
        p.pose.orientation.w = pose[6]

        self._scene.add_box(name, p, (dimensions[0], dimensions[1], dimensions[2]))