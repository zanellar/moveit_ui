#!/usr/bin/env python

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt! interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. (More on these below)
##
## We also import `rospy`_ and some messages that we will use:
##

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class Planner(object):
  
  def __init__(self, group_name="manipulator", superros_node = None):
    '''change this value to the name of your robot arm planning group.
    name is defined in the demo.launch of your robot'''

    ## `moveit_commander` initializations:
    moveit_commander.roscpp_initialize(sys.argv) 

    ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
    ## the robot:
    robot = moveit_commander.RobotCommander()

    ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
    ## to the world surrounding the robot:
    scene = moveit_commander.PlanningSceneInterface()

    ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
    ## to one group of joints.  In this case the group is the joints in the Panda
    ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
    ## you should change this value to the name of your robot arm planning group.
    ## This interface can be used to plan and execute motions on the Panda:
    # for UR5:  group_name = "manipulator"
    group = moveit_commander.MoveGroupCommander(group_name)

    ## We create a `DisplayTrajectory`_ publisher which is used later to publish
    ## trajectories for RViz to visualize:
    display_trajectory_publisher = None
    if superros_node is not None:
      display_trajectory_publisher = superros_node.createPublisher('/move_group/display_planned_path', 
                                                                        moveit_msgs.msg.DisplayTrajectory,
                                                                        queue_size=20)
 
    
    ## Getting Basic Information
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^
    # name of the reference frame for this robot:
    planning_frame = group.get_planning_frame()
    print "@@@@@@@@@@@@ Reference frame: %s" % planning_frame

    # We can also print the name of the end-effector link for this group:
    eef_link = group.get_end_effector_link()
    print "@@@@@@@@@@@@ End effector: %s" % eef_link

    # We can get a list of all the groups in the robot:
    group_names = robot.get_group_names()
    print "@@@@@@@@@@@@ Robot Groups: @@@@@@@@@@@@@", robot.get_group_names()

    # Sometimes for debugging it is useful to print the entire state of the
    # robot:
    print "@@@@@@@@@@@@ Printing robot state @@@@@@@@@@@@@"
    print robot.get_current_state()
    print ""

    # Misc variables
    self.box_name = '' 
    self.group_name = group_name
    self.scene = scene
    self.group = group  
    self.superros_node = superros_node
    self.display_trajectory_publisher = display_trajectory_publisher
    self.planning_frame = planning_frame
    self.eef_link = eef_link
    self.group_names = group_names

    self.velocity_percentage = 0.5 
    self.acceleration_percentage = 0.8

  def all_close(self, goal, actual, tolerance):
    """
    Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
      for index in range(len(goal)):
        if abs(actual[index] - goal[index]) > tolerance:
          return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
      return Planner.all_close(self, goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
      return Planner.all_close(self, pose_to_list(goal), pose_to_list(actual), tolerance)
    return True

  def go_to_pose_goal(self, pose, wait=True):
    """
    Move to a desired Pose Goal for the end-effector. 
    @param: pose       A geometry_msgs of type Pose with the target pose
    @returns: bool

    EXAMPLE:
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = -1.2
    pose_goal.position.x = 0.6
    pose_goal.position.y = 0.1
    pose_goal.position.z = 0.35
    planner.go_to_pose_goal(pose_goal)

    """ 
 
    self.group.set_pose_target(pose)

    ## Now, we call the planner to compute the plan and execute it. 
    plan = self.group.go(wait) 

    # Calling `stop()` ensures that there is no residual movement
    if wait:
      self.group.stop()
      # It is always good to clear your targets after planning with poses.
      # Note: there is no equivalent function for clear_joint_value_targets()
      self.group.clear_pose_targets() 

    current_pose = self.group.get_current_pose().pose
    return Planner.all_close(self, pose, current_pose, 0.01)

  def go_to_joint_state(self, joint_goal):
    """
    Move to a Joint Goal.
    @param: joint_goal       A list of float with the target joints 
    @returns: bool

    EXAMPLE:
    joint_goal[0] = 0
    joint_goal[1] = -pi/2
    joint_goal[2] = pi/2
    joint_goal[3] = -pi/2
    joint_goal[4] = -pi/2
    joint_goal[5] = 0
    planner.go_to_joint_state(joint_goal)

    NOTE:
    When the robot's configuration is at a `singularity <https://www.quora.com/Robotics-What-is-meant-by-kinematic-singularity>`_  
    the first thing we can do is move it to a slightly better configuration.
    We can get the joint values from the group and adjust some of the values: 
    joint_goal = group.get_current_joint_values()
    """ 
 
    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    self.group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    self.group.stop() 
 
    current_joints = self.group.get_current_joint_values()
    return Planner.all_close(self, joint_goal, current_joints, 0.01)


  def plan_cartesian_path(self, waypoints): 
    """
    Plan (only, without motion) a Cartesian path directly by specifying a list of waypoints for the end-effector to go through.
    @param: waypoints       A list of geometry_msgs of type Pose with the waypoints for the end-effector 
    @returns: plan, fraction

    EXAMPLE:  
    waypoints = []
    wpose = group.get_current_pose().pose
    wpose.position.z -=  0.1  # First move up (z)
    wpose.position.x +=  0.2  # and sideways (y)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0.1  # Second move forward/backwards in (x)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.y -=  0.1  # Third move sideways (y)
    waypoints.append(copy.deepcopy(wpose))
    planner.plan_cartesian_path(waypoints) 
    """ 
    # We want the Cartesian path to be interpolated at a resolution of 1 cm
    # which is why we will specify 0.01 as the eef_step in Cartesian
    # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
    (plan, fraction) = self.group.compute_cartesian_path(
                                       waypoints,   # waypoints to follow
                                       0.01,        # eef_step
                                       0.0)         # jump_threshold
 
    return plan, fraction
 

  def display_trajectory(self, plan, superros_node = None):    
    """
    Displaying a Trajectory in RViz
    @param: plan       A planned cartesian path for the end-effector 
    @returns: plan, fraction

    EXAMPLE:  display_trajectory_publisher
    plan, _ = planner.plan_cartesian_path(waypoints)
    planner.display_trajectory(plan) 
    """ 

    ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
    ## We populate the trajectory_start with our current robot state to copy over
    ## any AttachedCollisionObjects and add our plan to the trajectory.
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = self.robot.get_current_state()
    display_trajectory.trajectory.append(plan)

    # Publish
    if self.superros_node is None:
      self.superros_node = superros_node
      self.display_trajectory_publisher = superros_node.createPublisher('/move_group/display_planned_path', 
                                                                        moveit_msgs.msg.DisplayTrajectory,
                                                                        queue_size=20)
    if display_trajectory_publisher is not None:
      self.display_trajectory_publisher.publish(display_trajectory)
 

  def execute_plan(self, plan):  
    """
    Executing a path that has already been computed 
    @param: plan       A planned cartesian path for the end-effector 
    @returns: plan, fraction

    EXAMPLE:  
    plan, _ = planner.plan_cartesian_path(waypoints)
    planner.execute_plan(plan) 

    NOTE: 
    The robot's current joint state must be within some tolerance of the
    ## first waypoint in the `RobotTrajectory`_
    """  
    self.group.execute(plan, wait=True)
  
  def velocity_scaling(self, percentage):
    """
    Scaling of the maximum velocity (from 0% to 100%)
    @param: percentage       A float between 0 and 1 
    @returns: plan, fraction

    EXAMPLE:  
    percentage = 0.5
    plan, _ = planner.velocity_scaling(percentage) 
    """ 
    self.velocity_percentage = percentage
    self.group.set_max_velocity_scaling_factor(percentage)

  def acceleration_scaling(self,percentage):
    """
    Scaling of the maximum acceleration (from 0% to 100%)
    @param: percentage       A float between 0 and 1 
    @returns: plan, fraction

    EXAMPLE:  
    percentage = 0.5
    plan, _ = planner.acceleration_scaling(percentage) 
    """ 
    self.acceleration_percentage = percentage
    self.group.set_max_acceleration_scaling_factor(percentage)


  def relative_translation(self, offset):
    """ 
    @param: offset       A list of 3 coordinates x,y,z in meters
    @returns: boolen
 
    """  
    pose = self.group.get_current_pose().pose 
    pose.position.x += offset[0]
    pose.position.y += offset[1]
    pose.position.z += offset[2] 
    return self.go_to_pose_goal(pose, wait=True)

  def relative_rotation(self, rpy):
    """ 
    @param: offset       A list of 3 angles roll, pitch, yaw in rad  
    @returns: boolen
 
    """  
    # TODO
    return

  def get_pose(self):
    """ 
    Provide the current wrist pose 
    @returns: a geometry_msgs of type Pose with the current pose
 
    """  
    return self.group.get_current_pose().pose

  def get_joints(self):
    """ 
    Provide the current joints configuration 
    @returns: a list of float (joints)
 
    """  
    return self.group.get_current_joint_values()